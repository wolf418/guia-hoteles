module.exports = function (grunt){
  //Proyecto de configuracion
  const sass = require("node-sass");
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    sass: {
        options: {
            implementation: sass,
            sourceMap: none
        },
        dist: {
           files: [{
             expand:true,
             cwd:'css',
             src: ['*.scss'],
             dest: 'css',
             ext: '.css'
           }]
        }
    }
});


  // Load the plugin that provides the "uglify" task.
  //grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-sass');

  // Default task(s).
//  grunt.registerTask('css', ['sass']);}
  grunt.registerTask('css', ['sass']);
};
