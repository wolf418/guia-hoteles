$(document).ready(function(){
 $('[data-bs-toggle="tooltip"]').tooltip();
 $('[data-bs-toggle="popover"]').popover();

});

$('#contacto').on('show.bs.modal', function (e) {
 console.log('El modal contacto se esta mostrando');
   $('#contactoBtn').removeClass('btn-outline-success');
   $('#contactoBtn').addClass('btn-info');
   $('#contactoBtn').prop('disabled', true);
});

$('#contacto').on('shown.bs.modal', function (e) {
 console.log('El modal contacto se mostro');

});

$('#contacto').on('hide.bs.modal', function (e) {
 console.log('El modal contacto se oculta');
 $('#contactoBtn').removeClass('btn-info');
});

$('#contacto').on('hidden.bs.modal', function (e) {
 console.log('El modal contacto se oculto');

 $('#contactoBtn').addClass('btn-outline-success');
 $('#contactoBtn').prop('disabled', false);

});
